package org.example;

import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<List<Integer>> list = Arrays.asList(
                List.of(1, 2, 3),
                List.of(),
                List.of(4, 5));

        Assert.assertNotNull(list);

        if (list == null || list.isEmpty()) {
            System.out.println("List is empty or null");
        }





        /**
         Задание 15.1

         Пишем тесты на RestAssured - на API https://catfact.ninja/
         Секции breeds & facts - кликабельные-раскрываемые,
         внутри можно будет увидеть какие досутпны endpoint-ы (функционал/методы api).

         Ознакомьтесь с документацией к endpoint-ам.
         Разработайте как минимум 5 тестовых сценариев на функциональность этого API.
         Для каждого из тестов проверяйте ожидаемый статус-код и content-type.

         Там где возможно - реализуйте проверки на значимые секции тела ответа

         (в одном из случаев получая значение с помощью JSONPath,
         в другом - с помощью десериализации нужной части json в класс-модель).

         Так же для каждого теста воспользуйтесь возможностью задавать к аннотации @Test человекочитаемое описание теста
         в аттрибуте аннотации description, например так:
         @Test(description="Позитивный сценарий покупки товара")
         или
         @Test(description="Проверка наличия информации о платеже в теле ответа при корректном запросе на оплату")
         **/

        /**
         * Для работы с API нужны зависимости testng и restassured**/

    }
}