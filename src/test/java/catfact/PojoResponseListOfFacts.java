package catfact;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PojoResponseListOfFacts {

        private String fact;
        private Integer length;


}
