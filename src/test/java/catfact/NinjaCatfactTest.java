package catfact;

import io.restassured.RestAssured;
import io.restassured.response.ResponseBodyExtractionOptions;
import jdk.jfr.Description;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;


public class NinjaCatfactTest {

    RequestSpecification requestSpecification = new RequestSpecification();
    ResponsSpecification responsSpecification = new ResponsSpecification();



//    RequestSpecBuilder specBuilder = new RequestSpecBuilder();
//    RequestSpecification requestSpecification = specBuilder.log(LogDetail.ALL).build();

//    RequestSpecification requestSpecification = new RequestSpecBuilder()
//            .log(LogDetail.ALL)
//            .build();


//    ResponseSpecification responseSpecification = new ResponseSpecBuilder()
//            .log(LogDetail.ALL)
//            .expectStatusCode(200)
//            .expectBody("data[0].country", Matchers.equalTo("Ethiopia"))
//            .expectContentType(ContentType.JSON)
//            .build();


    @Test
    @Description("Проверка значений внутри ответа")
    public void test() {
        //JsonPath
        RestAssured
                .given()
                .log().all()
                .baseUri("https://catfact.ninja/breeds")
                .when()
                .log().all()
                .get()
                .then()
                .log().all()
                .statusCode(200)
                .body("data[0].country", Matchers.equalTo("Ethiopia"))
                .body("data[3].pattern", Matchers.equalTo("All"));

    }

    // Проверить работы метода на корректное количество возвращаемых объектов
    //Extract.body извлечение и сохранение тела ответа
    @Test
    @Description("Проверить метода breeds на корректное количество возвращаемых объектов, по умолчанию возвращает 25 объектов")
    public void test2() {
        ResponseBodyExtractionOptions responsBody = RestAssured
                .given()
                //.log().all()
                .baseUri("https://catfact.ninja/breeds")
                .when()
                //.log().all()
                .get()
                .then()
                //.log().all()
                .statusCode(200)
                .extract().body();

        String countryFirst = responsBody.jsonPath().getString("data[0].country");
        String countryLast = responsBody.jsonPath().getString("data[24].country");
        Assert.assertNotNull(countryFirst);
        Assert.assertNotNull(countryLast);

//        System.out.println(countryFirst);
//        System.out.println(countryLast);
//        System.out.println(responsBody.asPrettyString());

    }


    //Сериалтзация
    @Test(description = "Проверить работы метода на корректное количество возвращаемых объектов")
    public void test3() {
        RestAssured
                .given()
                //.log().all()
                .baseUri("https://catfact.ninja/breeds")
                .when()
                //.log().all()
                .get()
                .then()
                //.log().all()
                .statusCode(200)
                .extract().body();
    }

    //Спецификация
    @Test
    @Description("Проверить работы метода на корректное количество возвращаемых объектов")
    public void checkingStatusCodeBirdMethod() {
        RestAssured
                .given()
                .spec(requestSpecification.requestSpecification)
                .baseUri("https://catfact.ninja/breeds")
                .param("limit", 1)
                .when()
                //.log().all()
                .get()
                .then()
                .spec(responsSpecification.responseSpecification)
                .extract().body();
    }

    //Проверка значений полей в поджо классе, с актуальным значениями вернувшимися в ответе
    @Test
    @Description("Проверить работу метода \"breeds\" на возврат обязательных полей")
    public void ReturnRequiredFieldsTheBirdMethod() {
        PojoResponseListOfBreeds responseBody = new PojoResponseListOfBreeds(
                "Abyssinian",
                "Ethiopia",
                "Natural/Standard",
                "Short",
                "Ticked");

        PojoResponseListOfBreeds actualResponseBody = RestAssured
                .given()
                //.spec(requestSpecification.requestSpecification)
                .baseUri("https://catfact.ninja/breeds")
                .param("limit", 1)
                .when()
                //.log().all()
                .get()
                .then()
                .spec(responsSpecification.responseSpecification)
                .extract().body().jsonPath().getObject("data[0]", PojoResponseListOfBreeds.class);
        Assert.assertEquals(responseBody,actualResponseBody);
    }

    //Тест с проверками на пустые значения
    @Test
    @Description("Проверить валидацию в ответе метод \"breeds\" поля не должны быть пустые")
    public void ValidationOfTheFieldsOfTheBirdsMethod(){
        PojoResponseListOfBreeds actualResponseBody = RestAssured
                .given()
                .baseUri("https://catfact.ninja/breeds")
                .when()
                .get()
                .then()
                .statusCode(200)
                .extract().body().jsonPath().getObject("data[0]", PojoResponseListOfBreeds.class);
        Assert.assertNotNull(actualResponseBody);
    }

    //Тест с проверками на пустые значения fact
    @Test
    @Description("Проверить валидацию в ответе метод \"fact\" поля не должны быть пустые")
    public void ValidationOfTheFieldsOfTheFactMethod(){
        ResponseBodyExtractionOptions responsBody = RestAssured
                .given()
                .baseUri("https://catfact.ninja/fact")
                .when()
                .get()
                .then()
                .statusCode(200)
                .extract().body();
        String fact = responsBody.jsonPath().get("fact");
        String length = responsBody.jsonPath().getString("length");
        Assert.assertNotNull(fact);
        Assert.assertNotNull(length);
    }

    //Тест с проверками на пустые значения factS
    @Test
    @Description("Проверить что объекты возвращаемые в методе \"facts\" не пустые")
    public void ValidationOfTheFieldsOfTheFactsMethod(){
        ResponseBodyExtractionOptions responsBody = RestAssured
                .given()
                .baseUri("https://catfact.ninja/facts")
                .when()
                .get()
                .then()
                .statusCode(200)
                .extract().body();
        List data = responsBody.jsonPath().getList("data");
        Assert.assertNotNull(data);
        if (data == null) {
            System.out.println("List is null");
        }
        if (data.isEmpty()) {
            System.out.println("List is empty");
        }
        else {
            System.out.println("List is not empty or null");
        }

    }

}
